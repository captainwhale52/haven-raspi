const exec = require('child_process').exec;

const WebSocketClient = require('websocket').client;

const client = new WebSocketClient();

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });

    connection.on('message', function(message) {
        console.log(message);
        if (message.type === 'utf8') {
            console.log("Received: '" + message.utf8Data + "'");

            const data = JSON.parse(message.utf8Data);

            console.log(data);
            const playback = exec(`node index.js ${data.message.name}`);
        }
    });
});

client.connect('ws://0.0.0.0:8000/ws/book/');