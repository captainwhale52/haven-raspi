const config = require('config-yml');

const AWS = require('aws-sdk');
const fs = require('fs');
const ts = require('tailstream');
const exec = require('child_process').exec;
const spawn = require('child_process').spawn;

const axios = require('axios');
const uniqid = require('uniqid');


const lex = new AWS.LexRuntime({
    region: 'us-east-1',
});
const uniqueUserId = uniqid();
let streaming = false;
let inputStream;
let experienceOn = false;


const setupStream = function() {
    streaming = true;
    inputStream = ts.createReadStream(config.provider.environment.REQUEST_FILE);

    const params = {
        botName: config.resources.Resources.Lex.BOT_NAME,
        botAlias: config.resources.Resources.Lex.BOT_ALIAS,
        userId: uniqueUserId,
        contentType: config.provider.environment.REQUEST_AUDIO_FORMAT,
        inputStream: inputStream
    };

    lex.postContent(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            process.exit(1);
        } else {
            console.log(data);
            fs.writeFile(config.provider.environment.RESPONSE_FILE, data.audioStream, function(err) {
                if (err) {
                    console.log(err);
                    process.exit(1);
                }
            });
            const playback = exec(`${config.provider.environment.AUDIO_PLAYBACK_APP} ${config.provider.environment.RESPONSE_FILE}`);
            playback.on('close', function(code) {
                console.log('-----');
                console.log('DONE PLAYING LEX RESPONSE...');
                console.log('-----');
                if (data.intentName === 'SelectHavenExperience' && data.dialogState === 'Fulfilled') {
                    let experienceAudioFile = null;
                    let experienceVideoStartTime;
                    // TODO-Karl: Handle cases with the factory pattern instead of if nor switch statement.
                    if (data.sessionAttributes['experience'].includes('heavenly')) {
                        experienceAudioFile = 'media/sound/heavenly_body.mp3';
                        experienceVideoStartTime = 0;
                    } else if (data.sessionAttributes['experience'].includes('water')) {
                        experienceAudioFile = 'media/sound/waterfall.mp3';
                        experienceVideoStartTime = 30;
                    } else if (data.sessionAttributes['experience'].includes('space')) {
                        experienceAudioFile = 'media/sound/space_dream.mp3';
                        experienceVideoStartTime = 60;
                    } else if (data.sessionAttributes['experience'].includes('test')) {
                        experienceAudioFile = 'media/sound/haven_test.mp3';
                        experienceVideoStartTime = 90;
                    }


                    if (experienceAudioFile) {
                        experienceOn = true;
                        // TODO-Karl: Select the light mode based on the experience mode.
                        const experienceLight = JSON.parse(fs.readFileSync('./media/light/space_dream.json'));
                        experienceLight.bulbs.forEach(bulb => {
                            consumeLightSequences(bulb.index, bulb.sequences);
                        });

                        setTimeout(function () {
                            const experienceVideo = spawn(config.provider.environment.VIDEO_PLAYBACK_APP, ['media/video/default.mp4', '-f', `--start-time=${experienceVideoStartTime}`]);
                            const experienceSound = exec(`${config.provider.environment.AUDIO_PLAYBACK_APP} ${experienceAudioFile}`);
                            experienceSound.on('close', function (code) {
                                experienceOn = false;
                                console.log('-----');
                                console.log('DONE PLAYING HAVEN EXPERIENCE...');
                                console.log('-----');
                                experienceVideo.kill('SIGINT');
                                resetLights();
                                askFeedback(data.sessionAttributes['username'], data.sessionAttributes['experience'])
                            });



                        }, 5000);
                    }
                } else if (data.intentName === 'AskHavenFeedback' && data.dialogState === 'Fulfilled') {
                    turnOffLights();
                    console.log('-----');
                    console.log('EXIT THE HAVEN!');
                    console.log('-----');
                } else {
                    streaming = false;
                    record();
                }
            });
        }
    });
};

const record = function() {
    console.log('-----');
    console.log('START RECORDING...');
    console.log('-----');
    const recording = exec(config.provider.environment.AUDIO_RECORD_COMMAND);
    recording.stderr.on('data', function(data) {
        console.log(data);
        if (!streaming) {
            setupStream();
        }
    });
    recording.on('close', function(code) {
        inputStream.done();
        console.log('-----');
        console.log('DONE RECORDING REQUEST...');
        console.log('-----');
        // exec(REMOVE_REQUEST_FILE);
    });
};


const greetUser = function(username) {
    resetLights();
    console.log('-----');
    console.log(`START HAVEN WITH THE USERNAME: ${username}`);
    console.log('-----');
    const params = {
        botName: config.resources.Resources.Lex.BOT_NAME,
        botAlias: config.resources.Resources.Lex.BOT_ALIAS,
        userId: uniqueUserId,
        contentType: config.provider.environment.REQUEST_TEXT_FORMAT,
        inputStream: `My name is ${username}`
    };

    lex.postContent(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            process.exit(1);
        } else {
            console.log(data);
            fs.writeFile(config.provider.environment.RESPONSE_FILE, data.audioStream, function(err) {
                if (err) {
                    console.log(err);
                    process.exit(1);
                }
            });
            const playback = exec(`${config.provider.environment.AUDIO_PLAYBACK_APP} ${config.provider.environment.RESPONSE_FILE}`);
            playback.on('close', function(code) {
                console.log('-----');
                console.log('DONE PLAYING LEX RESPONSE...');
                console.log('-----');
                streaming = false;
                record();
            });
        }
    });
};

const askFeedback = function(username, experience) {
    console.log('-----');
    console.log(`ASK FEEDBACK TO USERNAME: ${username}`);
    console.log('-----');
    const params = {
        botName: config.resources.Resources.Lex.BOT_NAME,
        botAlias: config.resources.Resources.Lex.BOT_ALIAS,
        userId: uniqueUserId,
        contentType: config.provider.environment.REQUEST_TEXT_FORMAT,
        inputStream: `${username} has completed ${experience}`
    };

    lex.postContent(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            process.exit(1);
        } else {
            console.log(data);
            fs.writeFile(config.provider.environment.RESPONSE_FILE, data.audioStream, function(err) {
                if (err) {
                    console.log(err);
                    process.exit(1);
                }
            });
            const playback = exec(`${config.provider.environment.AUDIO_PLAYBACK_APP} ${config.provider.environment.RESPONSE_FILE}`);
            playback.on('close', function(code) {
                console.log('-----');
                console.log('DONE PLAYING LEX RESPONSE...');
                console.log('-----');
                streaming = false;
                record();
            });
        }
    });
};


function consumeLightSequences(bulbIndex, lightSequences) {
    if (experienceOn && lightSequences.length > 0) {
        const currentSequenceDuration = lightSequences[0]['duration'];
        const bulbStatePath = config.resources.Resources.PhillipsHue.BULB_PATH.replace(config.resources.Resources.PhillipsHue.BULB_INDEX_REPLACEMENT, bulbIndex);
        axios({
            method:'put',
            url: `${config.resources.Resources.PhillipsHue.API_BASE_URL}${bulbStatePath}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${config.resources.Resources.PhillipsHue.ACCESS_TOKEN}`
            },
            responseType: 'json',
            data: {
                on: lightSequences[0]['on'],
                bri: lightSequences[0]['bri'], // range: 0 ~ 254
                sat: lightSequences[0]['sat'],   // range: 0 ~254
                hue: lightSequences[0]['hue'],  // range: 0 ~ 65535
                transitiontime: lightSequences[0]['transitiontime'] * 0.01, // unit: 1000ms
            }
        }).then((success) => {
            lightSequences.shift();
            if (experienceOn && lightSequences.length > 0) {
                setTimeout(() => {
                    consumeLightSequences(bulbIndex, lightSequences);
                }, currentSequenceDuration)
            }
            console.log(success.data);
        }).catch((error) => {
            console.log('CANNOT CONNECT TO PHILLIPS HUE LIGHT!');
        });
    }
}

function resetLights() {
    axios({
        method:'put',
        url: `${config.resources.Resources.PhillipsHue.API_BASE_URL}${config.resources.Resources.PhillipsHue.ALL_LIGHTS_PATH}`,
        responseType: 'json',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${config.resources.Resources.PhillipsHue.ACCESS_TOKEN}`
        },
        data: {
            on: config.resources.Resources.PhillipsHue.DEFAULT_ON,
            bri: config.resources.Resources.PhillipsHue.DEFAULT_BRI, // range: 0 ~ 254
            sat: config.resources.Resources.PhillipsHue.DEFULT_SAT,   // range: 0 ~254
            hue: config.resources.Resources.PhillipsHue.DEFAULT_HUE,  // range: 0 ~ 65535
            transitiontime: config.resources.Resources.PhillipsHue.DEFAULT_TRANSITION_TIME * 0.01, // unit: 1000ms
        }
    }).then((success) => {
        console.log(success.data);
    }).catch((error) => {
        console.log(error);
        console.log('CANNOT CONNECT TO PHILLIPS HUE LIGHT!');
    });
}

function turnOffLights() {
    axios({
        method:'put',
        url: `${config.resources.Resources.PhillipsHue.API_BASE_URL}${config.resources.Resources.PhillipsHue.ALL_LIGHTS_PATH}`,
        responseType: 'json',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${config.resources.Resources.PhillipsHue.ACCESS_TOKEN}`
        },
        data: {
            on: false
        }
    }).then((success) => {
        console.log(success.data);
    }).catch((error) => {
        console.log('CANNOT CONNECT TO PHILLIPS HUE LIGHT!');
    });
}


greetUser(process.argv[2]);
